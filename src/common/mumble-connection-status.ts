export enum MumbleConnectionStatus {
    DISCONNECTED = "disconnected",
    CONNECTING = "connecting",
    ERROR = "error",
    HEALTHY = "healthy",
}
