import { Initial1527490790000 } from "./1527490790000-initial";
import { DeleteSounds1527490792100 } from "./1527490792100-delete-sounds";
import { RenamePlaylistDescription1527661621857 } from "./1527661621857-rename-playlist-description";
import { AddEchoColumn1528214237445 } from "./1528214237445-add-echo-column";
import { Favorites1600110191589 } from "./1600110191589-favorites";
import { Telegram1602850657715 } from "./1602850657715-telegram";

export const migrations = [
    Initial1527490790000,
    DeleteSounds1527490792100,
    RenamePlaylistDescription1527661621857,
    AddEchoColumn1528214237445,
    Favorites1600110191589,
    Telegram1602850657715,
];
